/*Programador: L�pez A. Daniela V.
Programa: E01NumerosPares
Objetivo: Crear un programa que imprima los n�meros pares del 0 al 100
Fecha: 12 de octubre del 2021*/

#include <stdio.h>
#include <stdlib.h>

int main(){
    
    int numero = 0;
    #define int DIVISOR = 2;

    for (numero = 0; numero <= 100; numero ++){
         if (numero % 2 == 0){
               printf("%d, ", numero);
         }//FinIf         
    }//FinFor
    
    return 0;
}//FinFunci�nPrincipal


