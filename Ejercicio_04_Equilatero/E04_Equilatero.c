/*Programador: L�pez A. Daniela V.
Programa: E04Equil�tero
Objetivo: Crear un programa que calcule el per�metro de un tri�ngulo equil�tero
Fecha: 12 de octubre del 2021*/

#include <stdio.h>
#include <stdlib.h>

int main(){
    
    float lado = 0;
    float perimetro = 0;
    #define int CANTIDAD_LADOS 3
   
    printf("Programa que calcula el per�metro de un tri�ngulo equil�tero");
    printf("\nUn tri�ngulo equil�tero tiene la misma medida en todos sus lados");
    printf("\nIngresa la medida de uno de sus lados: ");
    scanf("%f", &lado);
    
    perimetro = lado * CANTIDAD_LADOS;
        
    printf("\nEl per�metro del tri�ngulo equil�tero es: ", perimetro);

    return 0;
}//FinFunci�nPrincipal
