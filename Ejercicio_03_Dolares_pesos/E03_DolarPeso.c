/*Programador: L�pez A. Daniela V.
Programa: E03DolaresPesos
Objetivo: Crear un programa que reciba como entrada un monto en d�lares (USD) y devuelva la cantidad equivalente en pesos mexicanos (MXN)
Fecha: 12 de octubre del 2021*/

#include <stdio.h>
#include <stdlib.h>

int main(){
    
    float dolar = 0;
    float peso = 0;
    #define float PRECIO_DOLAR 20.93
   
    printf("Programa que convierte un monto en d�lares (USD) a pesos mexicanos (MNX)");
    printf("\nIngresa una cantidad en d�lares: ");
    scanf("%f", &dolar);
    
    if(dolar > 0){
        
        peso = dolar * PRECIO_DOLAR;
        printf("\nLa cantidad de $%f", dolar);
        printf(" d�lares son $%f", peso);
        printf(" pesos mexicanos");
        
    } else {
        
        printf("\nLa cantidad de $0.00 d�lares son $0.00 pesos mexicanos");

    }//FinIf
    
    
    return 0;
}//FinFunci�nPrincipal
