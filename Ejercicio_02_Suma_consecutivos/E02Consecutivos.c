/*Programador: L�pez A. Daniela V.
Programa: E02Consecutivos
Objetivo: Crear un programa que imprima la suma de los n�meros consectivos desde el 1 hasta el ingresado por el usuario (siendo menor a 50)
Fecha: 12 de octubre del 2021*/

#include <stdio.h>
#include <stdlib.h>

int main(){
    
    int numero = 0;
    int numeroi = 1;
    int numeroj = 0;
    int suma = 0;
   
    printf("Programa que suma todos los enteros consecutivos desde el 1 hasta el ingresado por el usuario");
    printf("\nIngresa un n�mero entre 1 y 50: ");
    scanf("%d", &numero);
    
    if(numero <= 50 && numero > 0){
        
        for(numeroi = 1; numeroi = numero; numeroi ++){
            
            numeroj = numeroi + 1;
            suma = numeroi + numeroj;
            numeroi = numeroi + 1;
            
            
        }//FinFor
        
        printf("La suma de los n�meros consecutivos entre 1 y %d", numero);
        printf(" es: %d", suma);
    } else {
        
        printf("\nError. El n�mero es mayor a 50 � es menor a 1");

    }//FinIf
    
    
    return 0;
}//FinFunci�nPrincipal
