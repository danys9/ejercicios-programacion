/*Programador: L�pez A. Daniela V.
Programa: E05Isosceles
Objetivo: Crear un programa que calcule el per�metro de un tri�ngulo isosceles
Fecha: 12 de octubre del 2021*/

#include <stdio.h>
#include <stdlib.h>

int main(){
    
    float lado = 0;
    float base = 0;
    float perimetro = 0;
   
    printf("Programa que calcula el per�metro de un tri�ngulo isosceles");
    printf("\nUn tri�ngulo isosceles tiene la misma medida en dos de sus lados y una medida distinta para la base (m�s peque�a)");
    printf("\nIngresa la medida para dos de sus lados: ");
    scanf("%f", &lado);
    printf("\nIngresa la medida de la base: ");
    scanf("%f", &base);
    
    if(base < lado){
         
         perimetro = lado * 2 + base;
         printf("\nEl per�metro del tri�ngulo isosceles es:", perimetro);
            
    } else {
          printf("\nLa base no puede ser mayor que dos de sus lados");
    }
          
            
    return 0;
}//FinFunci�nPrincipal
