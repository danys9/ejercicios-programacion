/*Programador: L�pez A. Daniela V.
Programa: E06Escaleno
Objetivo: Crear un programa que calcule el per�metro de un tri�ngulo escaleno
Fecha: 12 de octubre del 2021*/

#include <stdio.h>
#include <stdlib.h>

int main(){
    
    float lado1 = 0;
    float lado2 = 0;
    float lado3 = 0;
    float perimetro = 0;
   
    printf("Programa que calcula el per�metro de un tri�ngulo escaleno");
    printf("\nUn tri�ngulo escaleno tiene distintas medidas para sus 3 lados");
    printf("\nIngresa la medida del lado 1: ");
    scanf("%f", &lado1);
    printf("\nIngresa la medida del lado 2: ");
    scanf("%f", &lado2);
    printf("\nIngresa la medida del lado 3: ");
    scanf("%f", &lado3);
   
    if(lado1 == lado2 || lado1 == lado3 || lado2 == lado3){
             
         printf("\nEl tri�ngulo no es escaleno, es isosceles");
            
    } else {
           if(lado1 == lado2 && lado1 == lado3){
                    
                    printf("El tri�ngulo no es escaleno, es equil�tero");
           } else {
                  
                  perimetro = lado1 + lado2 + lado3;
                  printf("El per�metro del tri�ngulo escaleno es: %f", perimetro);
           }
    }
                  
                         
    return 0;
}//FinFunci�nPrincipal
